var app = angular.module('test', ['ngRoute']);

app.controller('HomeController', function($scope) {
  $scope.view = {};
  $scope.view.message = 'Home View';
});

app.controller('SecondController', function($scope) {
  $scope.view = {};
  $scope.view.message = 'second view!';
});
