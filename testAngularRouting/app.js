var app = angular.module('test', ['ngRoute']);
app.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/home.html',
      controller: 'HomeController'
    })
    .when('/second', {
      templateUrl: 'partials/second.html',
      controller: 'SecondController'
    })
})
